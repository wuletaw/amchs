<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'toor');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UV*:og,t*Stn$_H73Jw2kT9raEz,:on,(,6B)PdPtDK~;^uA2{G}`wlPd~o9Tfqv');
define('SECURE_AUTH_KEY',  'NyH?e&%hl`=:[(UPv`0WEZ-gCO:}*yItcZ]y#H`c}}07PnC(^A3KdhBL/-A>U;z8');
define('LOGGED_IN_KEY',    '^B4B`~F9|,YBJ`4neKnURn9SHV2PwFKe{Nj*om5[Ki_5jzHOJ?,8oi7VrDcwXMq?');
define('NONCE_KEY',        'M{%Z8Btt4!|p}`(K]:@(<mJ[T L_4Q*zY)Bw?|C^q&^*YVOZr/[Jn9h,.]dw-)Fj');
define('AUTH_SALT',        '~):vMWCY>N<S,@`w6b4T3%C5oT,27]Sf[Jj0eDLsUzf/t9pHvN}DJeFjl_Z4ftli');
define('SECURE_AUTH_SALT', 'iiN!ZSVm98&YE5m-ItsAeI^SgCVm3B@CJH0F/gAv9q)+O<O)pH9m~%n%A!CAZ*& ');
define('LOGGED_IN_SALT',   '$:t0ku~}rF>f::[`8`tECkb+wv#Pb6Ohu^Zzj&{xNO{E!b`}tllJX=n`BSk6#_Ha');
define('NONCE_SALT',       'S+JVCY16JZ,VW.L6Gic=YQmIRJ+-W/YH>MzZ* yizJX9mM.&MDm;/9dSaWP3}hP;');

/* Configure proxy Server */
define('WP_PROXY_HOST', 'proxy.amu.edu.et');
define('WP_PROXY_PORT', '8080');
define('WP_PROXY_USERNAME', '');
define('WP_PROXY_PASSWORD', '');
define('WP_PROXY_BYPASS_HOSTS', 'localhost');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
