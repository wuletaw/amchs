<?php
/**
 * The header for our theme
 *
 * @package WordPress
 * @subpackage play-school-kindergarten
 * @since 1.0
 * @version 0.3
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="<?php echo esc_url( __( 'http://gmpg.org/xfn/11', 'play-school-kindergarten' ) ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'play-school-kindergarten' ); ?></a>

		<div class="top-header">
			<div class="container">	
				<?php get_template_part( 'template-parts/header/header', 'image' ); ?>
				<div class="row padd0 header-section">
					<div class="col-lg-8 col-md-8 top">
						<div class="row">
							<div class="col-lg-5 col-md-5">						
								<?php if( get_theme_mod( 'play_school_kindergarten_mail','' ) != '') { ?>	
							        <i class="fas fa-envelope"></i><span class="col-org"><?php echo esc_html( get_theme_mod('play_school_kindergarten_mail',__('support@example.com','play-school-kindergarten')) ); ?></span>
							    <?php } ?>
						   	</div>
						   	<div class="col-lg-7 col-md-7">				   	
						   		<?php if( get_theme_mod( 'play_school_kindergarten_location','' ) != '') { ?>		
							        <i class="fas fa-map-marker-alt"></i><span class="col-org"><?php echo esc_html( get_theme_mod('play_school_kindergarten_location',__('9870 st. vicent place, glasgaw ,D.C 45 Fr 45','play-school-kindergarten')) ); ?></span>
							    <?php } ?>
						   	</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 mag0">
						<div class="social-icon">
							<?php if( get_theme_mod( 'play_school_kindergarten_facebook','' ) != '') { ?>
								<a href="<?php echo esc_url( get_theme_mod('play_school_kindergarten_facebook','') ); ?>"><i class="fab fa-facebook-f"></i></a>
							<?php } ?>
							<?php if( get_theme_mod( 'play_school_kindergarten_twitter','' ) != '') { ?>
								<a href="<?php echo esc_url( get_theme_mod('play_school_kindergarten_twitter','') ); ?>"><i class="fab fa-twitter"></i></a>
							<?php } ?>
							<?php if( get_theme_mod( 'play_school_kindergarten_linkdin','' ) != '') { ?>
								<a href="<?php echo esc_url( get_theme_mod('play_school_kindergarten_linkdin','') ); ?>"><i class="fab fa-linkedin-in"></i></a>
							<?php } ?>
							<?php if( get_theme_mod( 'play_school_kindergarten_instagram','' ) != '') { ?>
								<a href="<?php echo esc_url( get_theme_mod('play_school_kindergarten_instagram','') ); ?>"><i class="fab fa-instagram"></i></a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="toggle"><a class="toggleMenu" href="#"><?php esc_html_e('Menu','play-school-kindergarten'); ?></a></div>

		<div id="header">
			<div class="container">
				<div class="main-top">
					<div class="row padd0">
						<div class="col-lg-4 col-md-4">
							<div class="logo">
						        <?php if( has_custom_logo() ){ play_school_kindergarten_the_custom_logo();
						           }else{ ?>
						          <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						          <?php $description = get_bloginfo( 'description', 'display' );
						          if ( $description || is_customize_preview() ) : ?> 
						            <p class="site-description"><?php echo esc_html($description); ?></p>       
						        <?php endif; }?>
						    </div>
						</div>
						<div class="nav col-lg-6 col-md-6">
							<?php wp_nav_menu( array('theme_location'  => 'primary') ); ?>	
						</div>
						<div class="col-lg-2 col-md-2">
							<?php if( get_theme_mod( 'play_school_kindergarten_button_link','' ) != '') { ?>
			         		<div class="courses">
			           			<a href="<?php echo esc_url( get_theme_mod('play_school_kindergarten_button_link','#' ) ); ?>"><?php esc_html_e( 'Our Courses','play-school-kindergarten' ); ?></a>
			        		</div>
			         		<?php } ?>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</body>