<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage play-school-kindergarten
 * @since 1.0
 * @version 1.4
 */

?>
<div class="site-info">
	<p><?php echo esc_html(get_theme_mod('play_school_kindergarten_footer_copy',__('Kindergarten WordPress Theme Design By','play-school-kindergarten'))); ?> <?php play_school_kindergarten_credit(); ?></p>
</div>
